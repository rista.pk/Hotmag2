	$('.carousel .item').each(function(){
		var next = $(this).next();
		if (!next.length) {
			next = $(this).siblings(':first');
		}
		next.children(':first-child').clone().appendTo($(this));
			  
		for (var i=0;i<1;i++) {
			next=next.next();
			if (!next.length) {
			    next = $(this).siblings(':first');
			} 
		next.children(':first-child').clone().appendTo($(this));
		}
	});

		function postHover(){
				$(".one-post").hover(function() {
					$(this).addClass("overflow-hidden");
					$(this).height($(this).find('.image-caption').height());
					$(this).find('.image-caption').removeClass("col-sm-3");
					$(this).find('.image-caption').addClass("ic-add");
					//var capW = $(".none-caption").width();
					$(this).find('.caption-first').stop(true).animate({width: '40%'}, 300);
					$(this).find('.image-ff').addClass("col-sm-offset-3");
					$(this).find('.image-ff').css("transform", "translateX(-25%)");
					$(this).find('.oneolay').stop(true).animate({width: "100%"}, 300)
					$('.delete-it').fadeTo(100, 0, function(){
					   $('.delete-it').css("visibility", "hidden");   
					});
					$(this).find('.one-post-text').css("color","#1f1f1f");
					$(this).find('.caption-first').delay(900).html($(this).find('.none-caption').html());
				}, function(){
			        $(this).find('.image-caption').addClass("col-sm-3");
			        $(this).find('.caption-first').stop(true).animate({width: "34px"}, 300)
			        $(this).find('.oneolay').stop(true).animate({width: "0%"}, 300)
					$('.delete-it').fadeTo(200, 1, function(){
					   $('.delete-it').css("visibility", "visible");   
					});
					$(this).find('.one-post-text').css({"color":"#727272;"});
					$(this).find('.image-ff').css("transform", "translateX(0%)");
					$(this).find('.image-caption').removeClass("ic-add");
					$(this).find('.image-caption').addClass("ic-add2");
					$(this).find('.caption-first').html("F");

					$(this).delay(100).css("overflow", "auto");
			});
		}

	$(".one-cat").hover(function() {
			$(this).find('.cat-img').addClass("cat-fullimg");
			$(this).find('.cat-img').stop(true).animate({height:"204px"},500);
			$(this).find('.cat-descp').addClass("cat-fulldescp");
			$(this).find('.cat-overlay').addClass("cat-fullimg-o");
		}, function(){
	        $(this).find('.cat-img').removeClass("cat-fullimg");
	        $(this).find('.cat-img').stop(true).animate({height:"106px"},500);
	        $(this).find('.cat-descp').removeClass("cat-fulldescp");
	        $(this).find('.cat-overlay').removeClass("cat-fullimg-o");
	});

	$(".carousel-items").hover(function() {
		$(this).find(".carousel-over").stop(true).animate({width: "99%"}, 500);
		}, function(){
		$(this).find(".carousel-over").stop(true).animate({width: "0%"}, 500);
	});

	$(".one-popular").hover(function() {
		$(this).find(".sidebar-width-o").stop(true).animate({width: "100%"}, 300);
		}, function(){
		$(this).find(".sidebar-width-o").stop(true).animate({width: "0%"}, 300);
	});

	$(".oneolay").height($(".one-post").height());

	var mySwiper = new Swiper('.swiper-container',{
	    pagination: '.pagination',
	    autoplay: 3000,
	    loop:true,
	    grabCursor: true,
	    paginationClickable: true,
	});

	var mySwiper2 = new Swiper('.swiper-container2',{
	    loop:true
	});

	$(".grid-post").hover(function() {
		$(this).find('.gp-img').addClass("hoverswipA");
		$(this).find('.pluss').addClass("font-hover");
		$(this).find('.one-type').addClass("font-hover");
		$(this).find('.grid-text').addClass("font-hover");
	}, function(){
        $(this).find('.gp-img').removeClass("hoverswipA");
        $(this).find('.pluss').removeClass("font-hover");
        $(this).find('.one-type').removeClass("font-hover");
        $(this).find('.grid-text').removeClass("font-hover");
    });

	$('.arrow-left-sw').on( 'click', function() {
	    	mySwiper.swipePrev();
	});

	$('.arrow-right-sw').on( 'click', function() {
	    	mySwiper.swipeNext();
	});

	$('.arrow-left-sw2').on( 'click', function() {
	    	mySwiper2.swipePrev();
	});

	$('.arrow-right-sw2').on( 'click', function() {
	    	mySwiper2.swipeNext();
	});

	var swipH = $(".swiper-slide").width();
	$(".swiper-hover").width(swipH);

	$(".text-over-slider").css({
		"margin-top": -(($(".text-over-slider").height())/2)
	})

	$(".text-over-slider-footer").css({
		"margin-top": -((($(".text-over-slider").height())/2)+200)
	})


	//CALL ON WINDOW LOAD
	$(window).load(function () {

		var mediasize1 = 1183;
		if($(window).width() > mediasize1)
		{
		postHover();
		}

		var mediasize = 991;
		if($(window).width() < mediasize)
		{
			$(".gridd").height($(".gridd .row .col-md-4").height()*6);
			$(".swiper-container2").height($(".gridd").height());
		}
		else{
			$(".gridd").height($(".gridd .row").height()*2);
			$(".swiper-container2").height($(".gridd").height());
		}

		$(".full-menu").css({
			"margin-top": -($("header").height()),
			"height" : ($(".tp-banner").height()+$("header").height())
		});

		$(".full-menu > ul").css({
			"margin-top": -($(".full-menu > ul").height()/2),
			"margin-left" : -($(".full-menu > ul").width()/2)
		});

		$(".social-menu").css({
			"margin-top": (($(".full-menu > ul").height()/2)+50),
			"margin-left" : -($(".social-menu").width()/2)
		});

		$(".menuu").on('click', function(){
		     		$(".btn-menu").toggleClass('menu-opened');
		     		var mainmenu = $(".btn-menu").next('ul');
		     		if (mainmenu.hasClass('open')) { 
		       			mainmenu.slideToggle().removeClass('open');
		     		}
		     		else {
		       			mainmenu.slideToggle().addClass('open');
		     		}
		});


		$(".menuu").on('click', function(){
			$(".full-menu").toggleClass('menu-class');
			$(".menuu").toggleClass('bgMenu');
			$('.menuu p').html($('.menuu p').text() == 'CLOSE ' ? 'MENU' : 'CLOSE ');
			$(".full-menu > ul > li").each(function fmToggle(index){
				$(this).delay(index*200).queue(function() {
	              	$(this).toggleClass('menu-class1');
	             	$(this).dequeue(function() {
	             	});
	          	});
			});
		});

		$(".read-more h4").on('click', function(){
			$(".read-more h4").html("Open...");
		});

		$(".swiper-read-more span").on('click', function(){
			$(".swiper-read-more span").html("Open...");
		});

		$(".full-menu > ul > li").hover(function() {
			$(".full-menu > ul > li > a").addClass("hoverA");
		}, function(){
	        $(".full-menu > ul > li > a").removeClass("hoverA");
	    });

		$(".news").on('click', function(){
			$(".latest-posts-list").slideToggle();
		});
			
		$(".latest-posts-list").height($(".tp-banner").height()-20);

		$(".week-month-all > ul > li").on('click', function(){
			$(".week-month-all > ul > li > .arrow-down").stop(true).fadeOut("fast");
			$(this).find(".arrow-down").stop(true).fadeIn(20);
		});

		$('.search-button-trigger').on('click', function(e){
			e.preventDefault();
			if($(this).hasClass('opened')){
				var sPop = $('.search-popout-block').innerHeight();
				$('.search-popout-block').animate({top : -(sPop)}, 1000);
				$(this).removeClass('opened');
				
			}
			else{
				var sBack = $('header').height();	
				$('.search-popout-block').stop(true).animate({top : sBack}, 300,function(){
					$('.search-popout-block').find('input').trigger('focus');
					});
				$(this).addClass('opened');
				
			}
		});

		$('.has-sub').on('click', function(e){
			$(".submenu-b1").toggleClass("deleteDisp");
			$(".submenu-b1").fadeToggle(600);
			$( ".col-md-3" ).each(function( index, element ) {
				$(element).delay(200*(index+1)).slideToggle(450);			
			});	
		});

		$('#player').height($('#player').width()*0.56);

		var module = document.getElementsByClassName("grid-text");
			$clamp(module, {clamp: 3});

	});


	

